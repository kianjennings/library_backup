package dit.itunes.rest;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dit.itunes.entities.Playlist;
import dit.itunes.entities.Track;
import dit.itunes.service.PlaylistServiceInterface;

@Path("/playlist")
@Stateless
public class PlaylistREST {
	
	@EJB
	PlaylistServiceInterface playlistService;
	
	@GET
	@Path("get_playlist/{playlistID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Playlist getPlaylist(@PathParam("playlistID") String playlistID){
		return playlistService.getPlaylist(playlistID);
	}
	
	@GET
	@Path("/get_all/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Playlist> getPlaylists(@PathParam("username") String username){
		return playlistService.getPlaylists(username);
	}
	
	@GET
	@Path("/get_tracks/{uniquePlaylistID}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Track> getPlaylistTracks(@PathParam("uniquePlaylistID") String uniquePlaylistID){
		return playlistService.getPlaylistTracks(uniquePlaylistID);
	}
	
	@DELETE
	@Path("/del/track/{data}")
	public int removeTrackFromPlaylist(@PathParam("data") String data){
		try {
			String decodedData = java.net.URLDecoder.decode(data, "UTF-8");
			String[] splitData = decodedData.split("aaasssddd");
			String uniquePlaylistId = splitData[0];
			String uniqueTrackID = splitData[1];
			return playlistService.removeTrackFromPlaylist(uniquePlaylistId, uniqueTrackID);
		} catch(UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	@POST
	@Path("/move/{data}")
	public void moveTrackToPlaylists(@PathParam("data") String data){
		try{
			String decodedData = java.net.URLDecoder.decode(data, "UTF-8");
			playlistService.moveTrackToPlaylists(decodedData);
		}catch(UnsupportedEncodingException e){
			e.printStackTrace();
		}
	}
	
}
