package dit.itunes.rest;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dit.itunes.entities.Track;
import dit.itunes.service.PlaylistServiceInterface;
import dit.itunes.service.TrackServiceInterface;

@Path("/tracks")
@Stateless
public class TrackREST {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(TrackREST.class);
	
	@EJB
	TrackServiceInterface trackService;
	
	@EJB
	PlaylistServiceInterface playlistService;
	
	@GET
	@Path("/get_all/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Track> getTracks(@PathParam("username") String username){
		return trackService.getTracks(username);
	}
	
	@DELETE
	@Path("/del/{uniqueTrackID}")
	@Produces(MediaType.TEXT_HTML)
	public String removeTrack(@PathParam("uniqueTrackID") String uniqueTrackID){
		return trackService.removeTrack(uniqueTrackID);
	}
	
	@GET
	@Path("/edit/{data}")
	public void editTrack(@PathParam("data") String data){
		try {
			String decodedData = java.net.URLDecoder.decode(data, "UTF-8");
			trackService.editTrack(decodedData);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	@GET
	@Path("/search_album/{data}")
	public List<Track> getTracksByAlbum(@PathParam("data") String data){
		return trackService.getTracksByAlbum(data);
	}
	
	@GET
	@Path("/search_artist/{data}")
	public List<Track> getTracksByArtist(@PathParam("data") String data){
		return trackService.getTracksByArtist(data);
	}
}