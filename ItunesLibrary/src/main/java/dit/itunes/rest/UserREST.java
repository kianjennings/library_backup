package dit.itunes.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dit.itunes.entities.User;
import dit.itunes.service.UserServiceInterface;

@Path("/user")
@Stateless
public class UserREST {
	
	@EJB
	UserServiceInterface userService;
	

	@GET
	@Path("/getuser/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public User getUser(@PathParam("username") String username){
		return userService.getUser(username);
	}
	
	@GET
	@Path("/getall")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getUsers(){
		return userService.getUsers();
	}
	
	@POST
	@Path("/adduser")
	@Consumes(MediaType.APPLICATION_JSON)
	public void addUser(User user){
		userService.addUser(user);
	}
	
	@DELETE
	@Path("/deluser")
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteUser(User user){
		userService.deleteUser(user);
	}
	

}
