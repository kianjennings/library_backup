package dit.itunes.rest;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dit.itunes.service.LibraryServiceInterface;

@Path("/library")
@Stateless
public class LibraryREST {
	
	@EJB
	LibraryServiceInterface libraryService;
	
	@GET
	@Path("/get_details/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public String[] getLibrary(@PathParam("username") String username){
		return libraryService.getLibrary(username);
	}
	
}
