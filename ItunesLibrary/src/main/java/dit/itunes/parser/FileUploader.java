package dit.itunes.parser;

import java.io.File;
import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import dit.itunes.service.LibraryServiceInterface;

@SuppressWarnings("serial")
@WebServlet("/FileUploader")
@MultipartConfig(	fileSizeThreshold=1024*1024*2,	// 2MB 
					maxFileSize=1024*1024*10,		// 10MB
					maxRequestSize=1024*1024*50)	// 50MB
public class FileUploader extends HttpServlet{
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(FileUploader.class);
	
	private static final String SAVE_DIR = "tempXmlBackup";
	
	@EJB
	private LibraryServiceInterface libraryService;
	
	private SaxParser parser = new SaxParser();
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// gets absolute path of the web application
		String appPath = request.getServletContext().getRealPath("");
		// constructs path of the directory to save uploaded file
		String savePath = appPath + File.separator + SAVE_DIR;
		
		// creates the save directory if it does not exists
		File fileSaveDir = new File(savePath);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdir();
		}
		
		Part part = request.getPart("fileinput");
		String user = request.getParameter("username");
		String pass = request.getParameter("password");
		
		String fileName = extractFileName(part);
		String fileExtension = getFileExtension(fileName);
		
		boolean successful = true;
		
		if(fileExtension.equals(".xml")){
			parser.setDAOs(libraryService);
			if(user!=null && pass!=null){
				parser.setUser(user, pass);
			}else{
				return;
				//parser.setUser("null", "null");
			}
			String fileLocation = savePath+File.separator+fileName;
			part.write(fileLocation);
			File inputFile = new File(fileLocation);
			try {
				parser.parse(inputFile);
				inputFile.delete();
				Cookie name = new Cookie("username", user);
				response.addCookie(name);
			} catch (ParserConfigurationException | SAXException e) {
				inputFile.delete();
				e.printStackTrace();
			} catch (Exception e){
				inputFile.delete();
				successful = false;
			}
		}
		
		if(successful){
			response.sendRedirect("/ItunesLibrary/home.html");
		}else{
			Cookie alert = new Cookie("alert", "Upload Failed. Another user is using that xml file");
			response.addCookie(alert);
			response.sendRedirect("/ItunesLibrary/");
		}
	}
	

	
	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length()-1);
			}
		}
		return "";
	}
	
	static String getFileExtension(String fileName){
		int location = 0;
		for(int i=fileName.length()-1; i>0; i--){
			if(fileName.charAt(i)=='.'){
				location = i;
				break;
			}
		}
		return fileName.substring(location);
	}
}
