package dit.itunes.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.ejb.Stateless;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import dit.itunes.service.LibraryServiceInterface;

@Stateless
public class SaxParser {
	
	private LibraryServiceInterface libraryService;
	private String user;
	private String pass;
	
	public void parse(File inputFile) throws ParserConfigurationException, SAXException, IOException{
		SAXParserFactory saxFactory = SAXParserFactory.newInstance();
		SAXParser saxParser = saxFactory.newSAXParser();
		SaxHandler handler = new SaxHandler();
		InputStream xmlInputStream = new FileInputStream(inputFile);
		handler.setDAOs(libraryService);
		handler.setUser(user, pass);
		saxParser.parse(xmlInputStream, handler);
	}

	public void setDAOs(LibraryServiceInterface libraryService) {
		this.libraryService = libraryService;
	}

	public void setUser(String user, String pass) {
		this.user = user;
		this.pass = pass;
	}
}
