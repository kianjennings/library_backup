package dit.itunes.parser;

import java.text.ParseException;
import java.util.ArrayList;
import javax.ejb.Stateless;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import dit.itunes.entities.Library;
import dit.itunes.entities.Playlist;
import dit.itunes.entities.Track;
import dit.itunes.entities.User;
import dit.itunes.service.LibraryServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class SaxHandler extends DefaultHandler{
	
	//TODO figure out why some tracks are given track id of null
	
	private int location;
	private String key;
	private String value;
	
	private Library library;
	private ArrayList<Track> tracks;
	private ArrayList<Playlist> playlists;
	private User user;

	ArrayList<Integer> tempEntered = new ArrayList<Integer>();
	ArrayList<Integer> tempExited = new ArrayList<Integer>();
	
	Track track;
	Playlist playlist;

	private int trackNullCount = 0;
	private int playlistNullCount = 0;
	private LibraryServiceInterface libraryService;
	
	private static final Logger log = LoggerFactory.getLogger(SaxHandler.class);
	
	private void resetVariables(){
		library = new Library();
		tracks = new ArrayList<Track>();
		playlists = new ArrayList<Playlist>();
		track = new Track();
		playlist = new Playlist();
		

		location = 0;
		key = null;
		value = "";
	}
	
	public void startDocument() throws SAXException{
		log.info("starting xml parse");
		resetVariables();
	}
	

	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void endDocument() throws SAXException{
		log.info(trackNullCount + " tracks with a null id");
		log.info(playlistNullCount + " playlists with a null id");
		log.info("starting persistence from xml");
		user.setLibraryPersistentID(library.getLibraryPersistentID());
		library.setUser(user);
		if(library.getLibraryPersistentID()==null){
			log.info("Error, library has no @Id value");
		}
		libraryService.addLibrary(library);
		log.info("finished persistence from xml");
		
		resetVariables();
	}
	
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException{
		if(qName.equals("dict")){
			location++;
		}else if(qName.equals("key")){
			if(key!=null){
				addValues(key, value, location);
				key = null;
				value = "";
			}
		}else if(qName.equals("array")){
			if(location<4){
				location=4;
			}else{
				location++;
			}
		}else if(qName.equals("true") || qName.equals("false")){
			addValues(key, qName, location);
		}
		if(!(tempEntered.contains(location))){
			tempEntered.add(location);
		}
	}
	
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(qName.equals("dict")){
			switch(location){
			case 3:
				//end of track
				if(!(track.getTrackID()==null)){
					tracks.add(track);
				}else{
					trackNullCount++;
				}
				track = new Track(library.getLibraryPersistentID());
				key = null;
				value = "";
				break;
				
			case 2:
				library.setTracks(tracks);
				break;
				
			case 5:
				//end of playlist
				if(!(playlist.getPlaylistID()==null)){
					playlists.add(playlist);
				}else{
					playlistNullCount++;
				}
				playlist = new Playlist();
				key = null;
				value = "";
				break;
			}
			addValues(localName, qName, location);
			location--;
		}else if(qName.equals("array")){
			switch(location){
			case 4:
				//end of playlists
				library.setPlaylists(playlists);
				break;
			}
			location--;
		}
		if(!(tempExited.contains(location))){
			tempExited.add(location);
		}
	}
	
	public void characters(char ch[], int start, int length) throws SAXException{
		if(key==null){
			key = new String(ch, start, length);
		}else{
			value = value + new String(ch, start, length);
			/*addValues(key, value, location);*/
		}
	}
	
	public void ignorableWhitespace(char ch[], int start, int length)throws SAXException{
	}
	
	public void addValues(String key, String value, int location){
		switch(location){
		case 1:
			//library
			switch(key){
				case("Date"):
					try {
						library.setDate(value);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					break;
					
				case("Library Persistent ID"):
					library.setLibraryPersistentID(value);
					track.setLibraryPersistenceID(value);
					break;
			}
			break;
		case 3:
			//track
			switch(key){
				case ("Track ID"):
					track.setTrackID(Integer.parseInt(value));
					break;
				case ("Name"):
					track.setName(value);
					break;
				case ("Artist"): 
					track.setArtist(value);
					break;
				case ("Album"): 
					track.setAlbum(value);
					break;
				case ("Genre"): 
					track.setGenre(value);
					break;
				case ("Play Count"): 
					track.setPlayCount(Integer.parseInt(value));
					break;
				case ("Persistent ID"): 
					track.setPersistentID(value);
					break;
			}
			
			break;
		case 5:
			//playlist
			switch(key){
			case("Name"):
				playlist.setName(value);
				break;
			
			case("Playlist ID"):
				playlist.setPlaylistID(Integer.parseInt(value));
				playlist.setUniquePlaylistID(library.getLibraryPersistentID(), Integer.parseInt(value));
				/*log.info(playlist.getUniquePlaylistID());*/
				break;
			
			case("Playlist Persistent ID"):
				playlist.setPlaylistPersistentID(value);
				break;
				
			}
		case 7:
			//track on playlist
			switch(key){
			case("Track ID"):
				if(value==""){
					log.info("null");
				}
				int index = tracks.indexOf(new Track(Integer.parseInt(value)));
				if(index!=-1){
					Track t = tracks.get(index);
					playlist.addTrack(t);
					track = new Track(library.getLibraryPersistentID());
					key = null;
					value = "";
				}
			}
			break;
		}
	}
	
	public void setDAOs(LibraryServiceInterface libraryService) {
		this.libraryService = libraryService;
	}


	public void setUser(String username, String password) {
		user = new User(username, password);
	}
}
