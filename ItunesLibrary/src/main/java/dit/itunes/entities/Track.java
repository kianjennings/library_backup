package dit.itunes.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name="tracks")
public class Track {
	@Id
	private String uniqueTrackID;
	private Integer trackID;
	private String libraryPersistenceID;
	private String name;
	private String artist;
	private String album;
	private String genre;
	private Integer playCount;
	private String persistentID;
	
	public Track(){
		
	}
	public Track(int id){
		this.trackID = id;
	}
	public Track(String libraryPersistenceID){
		this.libraryPersistenceID = libraryPersistenceID;
	}
	public Track(int id, String name, String artist, String album, String genre, int playCount){
		this.trackID = id;
		this.name = name;
		this.artist = artist;
		this.album = album;
		this.genre = genre;
		this.playCount = playCount;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((trackID == null) ? 0 : trackID.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Track other = (Track) obj;
		if (trackID == null) {
			if (other.trackID != null)
				return false;
		} else if (!trackID.equals(other.trackID))
			return false;
		return true;
	}
	
	public Integer getTrackID() {
		return trackID;
	}
	public void setTrackID(Integer trackID) {
		this.trackID = trackID;
		this.uniqueTrackID = libraryPersistenceID + "-:-" + trackID;
	}
	public String getUniqueTrackID() {
		return uniqueTrackID;
	}
	public void setUniqueTrackID(String uniqueTrackID) {
		this.uniqueTrackID = uniqueTrackID;
	}
	public String getLibraryPersistenceID() {
		return libraryPersistenceID;
	}
	public void setLibraryPersistenceID(String libraryPersistenceID) {
		this.libraryPersistenceID = libraryPersistenceID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getAlbum() {
		return album;
	}
	public void setAlbum(String album) {
		this.album = album;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public Integer getPlayCount() {
		return playCount;
	}
	public void setPlayCount(Integer playCount) {
		this.playCount = playCount;
	}
	public String getPersistentID() {
		return persistentID;
	}
	public void setPersistentID(String persistentID) {
		this.persistentID = persistentID;
	}
}
