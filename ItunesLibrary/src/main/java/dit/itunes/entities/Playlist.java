package dit.itunes.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity 
@Table(name="playlists")
public class Playlist {
	private String name;
	@Id
	private String uniquePlaylistID;
	private Integer playlistID;
	private String libraryPersistentID;
	private String playlistPersistentID;
	
	@ManyToMany(cascade = CascadeType.PERSIST, fetch=FetchType.EAGER)
	private List<Track> tracks = new ArrayList<Track>();
	
	public String getUniquePlaylistID() {
		return uniquePlaylistID;
	}
	
	public void removeTrack(Track t){
		tracks.remove(t);
	}
	
	public void setUniquePlaylistID(String uniquePlaylistId) {
		this.uniquePlaylistID = uniquePlaylistId;
	}
	public void setUniquePlaylistID(String libraryPersistentId, Integer playlistId) {
		this.libraryPersistentID = libraryPersistentId;
		this.uniquePlaylistID = libraryPersistentId + "-:-"+ playlistId;
	}
	
	public String getLibraryPersistentID() {
		return libraryPersistentID;
	}

	public void setLibraryPersistentID(String libraryPersistentID) {
		this.libraryPersistentID = libraryPersistentID;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPlaylistID() {
		return playlistID;
	}
	public void setPlaylistID(Integer playlistID) {
		this.playlistID = playlistID;
	}
	public String getPlaylistPersistentID() {
		return playlistPersistentID;
	}
	public void setPlaylistPersistentID(String playlistPersistentID) {
		this.playlistPersistentID = playlistPersistentID;
	}
	
	public void addTrack(Track track){
		tracks.add(track);
	}
}
