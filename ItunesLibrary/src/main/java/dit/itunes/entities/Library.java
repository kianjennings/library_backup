package dit.itunes.entities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity 
@Table(name="libraries")
public class Library {
	
	private Date date;
	@Id
	private String libraryPersistentID;
	@OneToOne(cascade = CascadeType.PERSIST)
	private User user;
	@OneToMany(cascade = CascadeType.PERSIST)
	private List<Track> tracks;
	@OneToMany(cascade = CascadeType.PERSIST)
	private List<Playlist> playlists = null;
	
	public Date getDate() {
		return date;
	}
	public void setDate(String value) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		Date dateValue = sdf.parse(value);
		this.date = dateValue;
	}
	
	public String getLibraryPersistentID() {
		return libraryPersistentID;
	}
	public void setLibraryPersistentID(String libraryPersistentID) {
		this.libraryPersistentID = libraryPersistentID;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	public List<Track> getTracks() {
		return tracks;
	}
	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}
	
	public List<Playlist> getPlaylists() {
		return playlists;
	}
	public void setPlaylists(List<Playlist> playlists){
		this.playlists = playlists;
	}
	
	public void addPlaylist(Playlist playlist) {
		playlists.add(playlist);
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}
