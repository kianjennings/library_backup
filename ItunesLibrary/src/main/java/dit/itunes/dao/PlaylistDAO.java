package dit.itunes.dao;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dit.itunes.entities.Playlist;
import dit.itunes.entities.Track;
import dit.itunes.entities.User;

@Stateless
@Local
public class PlaylistDAO implements PlaylistDAOInterface{
	
	@PersistenceContext
	EntityManager em;
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(PlaylistDAO.class);
	
	@Override
	public List<Playlist> getPlaylists(String username) {
		User u = em.find(User.class, username);
		Query q = em.createQuery("FROM Playlist p WHERE p.libraryPersistentID = :libraryPersistentID ORDER BY name ASC");
		q.setParameter("libraryPersistentID", u.getLibraryPersistentID());
		@SuppressWarnings("unchecked")
		List<Playlist> result = q.getResultList();
		return result;
	}

	@Override
	public List<Track> getPlaylistTracks(String uniquePlaylistID) {
		Query q = em.createQuery("select tracks from Playlist where uniquePlaylistID = :uniquePlaylistID");
		q.setParameter("uniquePlaylistID", uniquePlaylistID);
		@SuppressWarnings("unchecked")
		List<Track> result = q.getResultList();
		return result;
	}

	@Override
	public int removeTrackFromPlaylists(String uniqueTrackID) {
		Query q = em.createNativeQuery("delete from playlists_tracks where tracks_uniqueTrackID = ?");
		q.setParameter(1, uniqueTrackID);
		int result = q.executeUpdate();
		em.flush();
		return result;
	}

	@Override
	public int removeTrackFromPlaylist(String uniquePlaylistId, String uniqueTrackId) {
		Query q = em.createNativeQuery("delete from playlists_tracks where playlists_uniquePlaylistID = :uniquePlaylistId AND tracks_uniqueTrackID = :uniqueTrackId");
		q.setParameter("uniquePlaylistId", uniquePlaylistId);
		q.setParameter("uniqueTrackId", uniqueTrackId);
		int result = q.executeUpdate();
		return result;
	}

	@Override
	public void moveTrackToPlaylists(String username, String olduniquePlaylistID, String newPlaylistName, int trackID) {
		User u = em.find(User.class, username);
		Query q = em.createQuery("FROM Playlist WHERE name = :playlistname AND libraryPersistentID = :libraryPersistentID");
		q.setParameter("playlistname", newPlaylistName);
		q.setParameter("libraryPersistentID", u.getLibraryPersistentID());
		Playlist pNew = (Playlist) q.getSingleResult();
		Track t = em.find(Track.class, u.getLibraryPersistentID() + "-:-" + trackID);
		pNew.addTrack(t);
		Playlist pOld = em.find(Playlist.class, olduniquePlaylistID);
		pOld.removeTrack(t);
		em.merge(pOld);
		em.merge(pNew);
	}

	@Override
	public Playlist getPlaylist(String playlistID) {
		return em.find(Playlist.class, playlistID);
	}
}
