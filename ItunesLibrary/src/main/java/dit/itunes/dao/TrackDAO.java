package dit.itunes.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dit.itunes.entities.Track;
import dit.itunes.entities.User;

@Stateless
@Local
public class TrackDAO implements TrackDAOInterface{
	
	private static final Logger log = LoggerFactory.getLogger(TrackDAO.class);
	
	@PersistenceContext
	EntityManager em;

	@EJB
	PlaylistDAOInterface playlistDAO;
	@EJB
	LibraryDAOInterface libraryDAO;

	@Override
	public String removeTrack(String uniqueTrackID) {
		int result = playlistDAO.removeTrackFromPlaylists(uniqueTrackID);
		result += libraryDAO.removeTrack(uniqueTrackID);
		Query q = em.createQuery("delete from Track t where t.uniqueTrackID = :uniqueTrackID");
		q.setParameter("uniqueTrackID", uniqueTrackID);
		log.info("tracks removed from playlists and libraries = " + result);
		result += q.executeUpdate();
		return "Total references deleted: " + result;
	}

	@Override
	public void addTracks(List<Track> tracks) {
		for(Track track : tracks){
			em.merge(track);
		}
	}

	@Override
	public List<Track> getTracks(String username) {
		Query q = em.createQuery("SELECT l.tracks FROM Library l WHERE l.libraryPersistentID = :libraryPersistentID ORDER BY trackID ASC");
		User u = em.find(User.class, username);
		q.setParameter("libraryPersistentID", u.getLibraryPersistentID());
		@SuppressWarnings("unchecked")
		List<Track> result = q.getResultList();
		return result;
	}

	@Override
	public void editTrack(String id, String name, String artist, String album, String genre, String play_count) {
		Track tOnDatabase = em.find(Track.class, id);
		tOnDatabase.setName(name);
		tOnDatabase.setArtist(artist);
		tOnDatabase.setAlbum(album);
		tOnDatabase.setGenre(genre);
		try{
			tOnDatabase.setPlayCount(Integer.parseInt(play_count));
		}catch(NumberFormatException e){
			//prevent adding of non number value and allowing blank value
			tOnDatabase.setPlayCount(null);
		}
		em.merge(tOnDatabase);
	}

	@Override
	public List<Track> getTracksByAlbum(String username, String album) {
		Query q = em.createQuery("SELECT t FROM Library l join l.tracks t WHERE l.libraryPersistentID = :libraryPersistentID AND t.album = :album ORDER BY t.trackID ASC");
		User u = em.find(User.class, username);
		q.setParameter("libraryPersistentID", u.getLibraryPersistentID());
		q.setParameter("album", album);
		@SuppressWarnings("unchecked")
		List<Track> result = q.getResultList();
		return result;
	}
	
	@Override
	public List<Track> getTracksByArtist(String username, String artist) {
		Query q = em.createQuery("SELECT t FROM Library l join l.tracks t WHERE l.libraryPersistentID = :libraryPersistentID AND t.artist = :artist ORDER BY t.trackID ASC");
		User u = em.find(User.class, username);
		q.setParameter("libraryPersistentID", u.getLibraryPersistentID());
		q.setParameter("artist", artist);
		@SuppressWarnings("unchecked")
		List<Track> result = q.getResultList();
		return result;
	}
}
