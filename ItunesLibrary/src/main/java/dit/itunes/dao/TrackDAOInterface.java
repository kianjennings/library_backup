package dit.itunes.dao;

import java.util.List;

import dit.itunes.entities.Track;

public interface TrackDAOInterface {
	
	String removeTrack(String uniqueTrackId);

	void addTracks(List<Track> tracks);

	List<Track> getTracks(String libraryPersistentID);

	void editTrack(String id, String name, String artist, String album,
			String genre, String play_count);

	List<Track> getTracksByAlbum(String username, String album);

	List<Track> getTracksByArtist(String username, String artist);

}
