package dit.itunes.dao;

import java.util.List;
import java.util.Objects;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dit.itunes.entities.Library;
import dit.itunes.entities.Track;
import dit.itunes.entities.User;

@Stateless
@Local
public class LibraryDAO implements LibraryDAOInterface{
	
	@PersistenceContext
	EntityManager em;

	private static final Logger log = LoggerFactory.getLogger(LibraryDAO.class);
	
	@Override
	public void addLibrary(Library library) {
		em.persist(library);
	}

	@Override
	public int removeTrack(String uniqueTrackID) {
		Query q = em.createNativeQuery("delete from libraries_tracks where tracks_uniqueTrackID = ?");
		q.setParameter(1, uniqueTrackID);
		
		log.info("uniqueTrackID = " + uniqueTrackID);
		
		int result = q.executeUpdate();
		log.info(result + " rows changed");
		em.flush();
		return result;
	}

	@Override
	public String[] getLibrary(String username) {
		Query q = em.createQuery("SELECT l.user.username, l.libraryPersistentID, l.date FROM Library l WHERE l.libraryPersistentID = :libraryPersistentID");
		User u = em.find(User.class, username);
		q.setParameter("libraryPersistentID", u.getLibraryPersistentID());
		@SuppressWarnings("unchecked")
		List<Object[]> result = q.getResultList();
		Object[] obj = result.get(0);
		/*String[] finalResult = new String[3];
		for(int i=0; i<3; i++){
			finalResult[i] = Objects.toString(obj[i]);
			log.info("#" + finalResult[i] + "#");
		}*/
		String[] finalResult = {Objects.toString(obj[0]), Objects.toString(obj[1]), Objects.toString(obj[2])};
		
		return finalResult;
	}
}
