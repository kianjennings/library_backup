package dit.itunes.dao;

import java.util.List;

import dit.itunes.entities.Playlist;
import dit.itunes.entities.Track;

public interface PlaylistDAOInterface {

	List<Playlist> getPlaylists(String username);
	
	List<Track> getPlaylistTracks(String uniquePlaylistID);
	
	int removeTrackFromPlaylists(String uniqueTrackID);
	
	int removeTrackFromPlaylist(String uniquePlaylistID, String uniqueTrackID);
	
	void moveTrackToPlaylists(String username, String olduniquePlaylistID, String newPlaylistname, int trackID);

	Playlist getPlaylist(String playlistID);
}
