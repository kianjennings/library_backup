package dit.itunes.dao;

import dit.itunes.entities.Library;

public interface LibraryDAOInterface {
	
	void addLibrary(Library library);

	int removeTrack(String uniqueTrackId);

	String[] getLibrary(String username);

}
