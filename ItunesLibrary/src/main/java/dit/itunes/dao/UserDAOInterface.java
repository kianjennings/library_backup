package dit.itunes.dao;

import java.util.List;

import dit.itunes.entities.User;

public interface UserDAOInterface {
	
	void addUser(User user);

	User getUser(String username);

	List<User> getUsers();

	void deleteUser(User user);

}
