package dit.itunes.dao;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dit.itunes.entities.User;

@Stateless
@Local
public class UserDAO implements UserDAOInterface{
	
	@PersistenceContext
	EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public User getUser(String username) {
		Query query = em.createQuery("from User u where u.username = :username");
		query.setParameter("username", username);
		List<User> result = query.getResultList();
		if(result.size()==0){
			return new User(null, null);
		}
		return result.get(0);
	}

	@Override
	public void addUser(User user) {
		em.merge(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUsers() {
		Query q = em.createQuery("from User");
		List<User> result = q.getResultList();
		return result;
	}

	@Override
	public void deleteUser(User user) {
		//TODO do i need a delete user option
	}
}
