package dit.itunes.service;

import java.util.List;

import dit.itunes.entities.Playlist;
import dit.itunes.entities.Track;

public interface PlaylistServiceInterface {
	
	List<Playlist> getPlaylists(String username);

	List<Track> getPlaylistTracks(String uniquePlaylistID);

	int removeTrackFromPlaylist(String uniquePlaylistID, String uniqueTrackID);

	int removeTrackFromPlaylists(String uniqueTrackID);

	void moveTrackToPlaylists(String decodedData);

	Playlist getPlaylist(String playlistID);

}
