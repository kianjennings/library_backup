package dit.itunes.service;

import dit.itunes.entities.Library;

public interface LibraryServiceInterface {

	void addLibrary(Library library);

	String[] getLibrary(String username);
}
