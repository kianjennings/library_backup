package dit.itunes.service;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import dit.itunes.dao.LibraryDAOInterface;
import dit.itunes.entities.Library;

@Stateless
public class LibraryService implements LibraryServiceInterface {
	
	@EJB
	LibraryDAOInterface libraryDAO;

	@Override
	public void addLibrary(Library library) {
		libraryDAO.addLibrary(library);
	}

	@Override
	public String[] getLibrary(String username) {
		return libraryDAO.getLibrary(username);
	}	
}
