package dit.itunes.service;

import java.util.List;

import dit.itunes.entities.Track;

public interface TrackServiceInterface {
	
	String removeTrack(String uniqueTrackID);
	
	void addTracks(List<Track> tracks);

	List<Track> getTracks(String libraryPersistentID);

	void editTrack(String data);

	List<Track> getTracksByAlbum(String album);

	List<Track> getTracksByArtist(String data);
}
