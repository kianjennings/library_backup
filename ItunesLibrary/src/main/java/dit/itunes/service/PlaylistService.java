package dit.itunes.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dit.itunes.dao.PlaylistDAOInterface;
import dit.itunes.entities.Playlist;
import dit.itunes.entities.Track;

@Stateless
public class PlaylistService implements PlaylistServiceInterface{
	
	@EJB
	PlaylistDAOInterface playlistDAO;
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(PlaylistService.class);

	@Override
	public List<Playlist> getPlaylists(String username) {
		return playlistDAO.getPlaylists(username);
	}

	@Override
	public List<Track> getPlaylistTracks(String uniquePlaylistID) {
		return playlistDAO.getPlaylistTracks(uniquePlaylistID);
	}
	
	@Override
	public int removeTrackFromPlaylist(String uniquePlaylistId, String uniqueTrackID) {
		return playlistDAO.removeTrackFromPlaylist(uniquePlaylistId, uniqueTrackID);
	}
	
	@Override
	public int removeTrackFromPlaylists(String uniqueTrackID){
		return playlistDAO.removeTrackFromPlaylists(uniqueTrackID);
	}

	@Override
	public void moveTrackToPlaylists(String decodedData) {
		String[] splitData = decodedData.split("aaasssddd");
		playlistDAO.moveTrackToPlaylists(splitData[0].trim(), splitData[1].trim(), splitData[2].trim(), Integer.parseInt(splitData[3].trim()));
	}

	@Override
	public Playlist getPlaylist(String playlistID) {
		return playlistDAO.getPlaylist(playlistID);
	}
}
