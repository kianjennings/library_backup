package dit.itunes.service;

import java.util.List;

import dit.itunes.entities.User;

public interface UserServiceInterface {

	User getUser(String username);

	List<User> getUsers();

	void addUser(User user);

	void deleteUser(User user);

}
