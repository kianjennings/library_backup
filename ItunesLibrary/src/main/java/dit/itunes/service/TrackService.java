package dit.itunes.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dit.itunes.dao.TrackDAOInterface;
import dit.itunes.entities.Track;

@Stateless
public class TrackService implements TrackServiceInterface {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(TrackService.class);
	
	@EJB
	TrackDAOInterface trackDAO;

	@Override
	public String removeTrack(String uniqueTrackID) {
		return trackDAO.removeTrack(uniqueTrackID);
	}

	@Override
	public void addTracks(List<Track> tracks) {
		trackDAO.addTracks(tracks);
	}

	@Override
	public List<Track> getTracks(String libraryPersistentID) {
		return trackDAO.getTracks(libraryPersistentID);
	}

	@Override
	public void editTrack(String data) {
		String[] splitData = data.split("aaasssddd");
		trackDAO.editTrack(splitData[0].trim(), splitData[1].trim(), splitData[2].trim(), splitData[3].trim(), splitData[4].trim(), splitData[5].trim());
	}

	@Override
	public List<Track> getTracksByAlbum(String data) {
		String[] splitData = data.split("-:-");
		return trackDAO.getTracksByAlbum(splitData[0], splitData[1]);
	}

	@Override
	public List<Track> getTracksByArtist(String data) {
		String[] splitData = data.split("-:-");
		return trackDAO.getTracksByArtist(splitData[0], splitData[1]);
	}
}
