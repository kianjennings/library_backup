package dit.itunes.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import dit.itunes.dao.UserDAOInterface;
import dit.itunes.entities.User;

@Stateless
public class UserService implements UserServiceInterface {

	@EJB
	UserDAOInterface userDAO;
	
	@Override
	public User getUser(String username) {
		return userDAO.getUser(username);
	}

	@Override
	public List<User> getUsers() {
		return userDAO.getUsers();
	}

	@Override
	public void addUser(User user) {
		userDAO.addUser(user);
	}

	@Override
	public void deleteUser(User user) {
		userDAO.deleteUser(user);
	}

}
